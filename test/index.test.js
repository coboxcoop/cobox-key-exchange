const { describe } = require('tape-plus')
const crypto = require('cobox-crypto')
const invite = require('../')

const swarmCores = []

const address = Buffer.from('b74717c149390c3e8e939f6e94169751d9ade8e2e21e0255ee593fdccb504603', 'hex')
const key = Buffer.from('peer2peerforeverpeer2peerforever') // 32 bytes

describe('encrypt and decrypt', (context) => {
  let keypair

  context.beforeEach((c) => {
    keypair = crypto.boxKeyPair()
  })

  context('without encryptionKey', (assert, next) => {
    const inviteCode = invite.create({ address, publicKey: keypair.publicKey })
    const output = invite.open(inviteCode, keypair)

    assert.same(output.address, address, 'correct address extracted from invite code')
    next()
  })

  context('without name', (assert, next) => {
    const inviteCode = invite.create({ encryptionKey: key, address, publicKey: keypair.publicKey })
    const output = invite.open(inviteCode, keypair)

    assert.same(output.encryptionKey, key, 'correct key extracted from invite code')
    assert.same(output.address, address, 'correct address extracted from invite code')
    next()
  })

  context('with name', (assert, next) => {
    const name = 'sausage dog'
    const inviteCode = invite.create({ encryptionKey: key, address, publicKey: keypair.publicKey, name })
    const output = invite.open(inviteCode, keypair)

    assert.same(output.encryptionKey, key, 'correct key extracted from invite code')
    assert.same(output.address, address, 'correct address extracted from invite code')
    assert.same(output.name, name, 'correct name extracted from invite code')
    next()
  })
})

// describe('swarm on a public key', (context) => {
//   let keypair, inviteCode, confirmSent

//   context.beforeEach((c) => {
//     keypair = crypto.boxKeyPair()
//     inviteCode = invite.create({ encryptionKey: key, address, publicKey: keypair.publicKey, name: 'slobdob' })
//     confirmSent = false
//   })

//   context('default', (assert, next) => {
//     // exchange.send(keypair.publicKey, inviteCode, { swarm: mockSwarm }, () => { confirmSent = true })
//     invite.send(keypair.publicKey, inviteCode, () => { confirmSent = true })

//     // exchange.receive(keypair.publicKey, { swarm: mockSwarm }, (err, output) => {
//     invite.receive(keypair.publicKey, (err, output) => {
//       assert.error(err, 'no error on receive')
//       assert.same(inviteCode, output, 'received correct invite code')
//       // assert.true(confirmSent, 'Sender can confirm invite code received')
//       next()
//     })
//   })
// })

function mockSwarm (core) {
  if (!swarmCores.includes(core)) {
    swarmCores.push(core)
    if (swarmCores.length === 2) {
      const streams = replicate(swarmCores[0], swarmCores[1])
      return streams.pop()
    }
  }
}

function replicate (feed1, feed2) {
  var s = feed1.replicate(true, { live: false })
  var d = feed2.replicate(false, { live: false })

  s.pipe(d).pipe(s)
  return [s, d]
}
