# cobox-key-exchange

Key exchange operations for cobox

## API
`const invite = require('cobox-key-exchange')`

### `const inviteCode = create(opts)`

`opts = { publicKey, address, [encryptionKey], [name] }`

Creates an invite code.  Returns a buffer.
- `publicKey` - the public encryption key to whom the invite will be encrypted.  Must be a buffer or hex encoded string.
- `address` - the address of the space to be included in the invite.  Must be a buffer or hex encoded string.
- Optional: `encryptionKey` - the encryption key to whom the invite will be encrypted.  Must be a buffer or hex encoded string.
- Optional: `name` - a name for the space.  Must be a string 

### `{ address, encryptionKey, name } = open(opts)`

`opts = { invite, keypair }`

Decrypts and unpacks an invite code.
- `inviteCode` should be a buffer.
- keypair should be an object of the form `{ publicKey, secretKey }` both of which must be buffers or hex encoded strings.

### `send(recipientKey, inviteCode, opts = {}, callback)`
- `recipientKey` - the public encryption key of the intended recipient of the invite. Must be a buffer or hex encoded string.
- `inviteCode` an invite code which should be a buffer
- `callback` a function which will be called when the invite has been transmitted. This may take some time depending on whether they are currently online.
- `opts` is an optional object which may include
  - `swarm` - a custom swarm function, which behaves similarly to `cobox-swarm`

### `receive(publicKey, opts = {}, callback)` 
- `publicKey` - the public encryption key of the recipient of the invite. Must be a buffer or hex encoded string.
- `callback(err, inviteCode)` a function which will be called when the invite has been received.
  - `inviteCode` an invite code which will be a buffer
- `opts` is an optional object which may include
  - `swarm` - a custom swarm function, which behaves similarly to `cobox-swarm`
