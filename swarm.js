const hyperswarm = require('hyperswarm')
const pump = require('pump')

module.exports = function (space, opts = {}) {
  var swarm = hyperswarm()

  swarm.on('connection', (socket, details) => {
    pump(socket, space.replicate(details.client, { live: false }), socket)
  })

  swarm.join(space.discoveryKey, { lookup: true, announce: true })

  return swarm
}
